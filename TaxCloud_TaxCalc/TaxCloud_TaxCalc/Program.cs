﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Newtonsoft.Json;
using TaxCloud_TaxCalc.net.taxcloud.api;
namespace TaxCloud_TaxCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                // string s = "{\"Header\":{\"City\": \"BLAIRSVILLE\",\"State\":\"PA\",\"Zip\":\"15717-8707\",\"Address\":\"344 KENDALL RD\",\"Frieght\":0,\"CustNum\":\"C000001\",\"OrderNum\":\"testCO0018\", \"OrderRef\":\"2F32AFE3-4AA7-42F5-B1C8-569EB3994317\" } }";
                //string s = " {\"Header\":{\"City\": \"BLAIRSVILLE\",\"State\":\"PA\",\"Zip\":\"15717-8707\",\"Address\":\"344 KENDALL RD\",\"Frieght\":0,\"CustNum\":\"C000001\",\"OrderNum\":\"testCO0018\", \"OrderRef\":\"2F32AFE3-4AA7-42F5-B1C8-569EB3994317\" },\"Detail\":[ {\"Itemcode\":\"APDD\",\"Qty\":\"1\",\"Price\":\"10\",\"TICCode\":\"00000\"},{\"Itemcode\":\"C12\",\"Qty\":\"1\",\"Price\":\"10\",\"TICCode\":\"00000\"},{\"Itemcode\":\"C23\",\"Qty\":\"1\",\"Price\":\"10\",\"TICCode\":\"00000\"}]}";
                // " {\"Header\":{\"City\": \"BLAIRSVILLE\",\"State\":\"PA\",\"Zip\":\"15717-8707\",\"Address\":\"344 KENDALL RD\",\"Frieght\":0,\"CustNum\":\"C000001\",\"OrderNum\":\"testCO0018\", \"OrderRef\":\"2F32AFE3-4AA7-42F5-B1C8-569EB3994317\", \"CertId\":\"NA\" },\"Detail\":[ {\"Itemcode\":\"APDD\",\"Qty\":\"1\",\"Price\":\"10\",\"TICCode\":\"00000\"},{\"Itemcode\":\"C12\",\"Qty\":\"1\",\"Price\":\"10\",\"TICCode\":\"00000\"},{\"Itemcode\":\"C23\",\"Qty\":\"1\",\"Price\":\"10\",\"TICCode\":\"00000\"}]}"
                // " {\"Header\":{\"City\": \"BLAIRSVILLE\",\"State\":\"PA\",\"Zip\":\"15717-8707\",\"Address\":\"344 KENDALL RD\",\"Frieght\":0,\"CustNum\":\"C000001\",\"OrderNum\":\"testCO0018\", \"OrderRef\":\"2F32AFE3-4AA7-42F5-B1C8-569EB3994317\", \"CertId\":\"NA\" ,\"invoiceno\":\"test-0001\"},\"Detail\":[ {\"Itemcode\":\"APDD\",\"Qty\":\"1\",\"Price\":\"10\",\"TICCode\":\"00000\"},{\"Itemcode\":\"C12\",\"Qty\":\"1\",\"Price\":\"10\",\"TICCode\":\"00000\"}]}"
                if (args[0].Length > 0)
                {

                    List<OrderLine> AllLines = new List<OrderLine>();

                    TaxCalculator taxcalc = new TaxCalculator();

                    dynamic orderInfo = JsonConvert.DeserializeObject(args[0]);
                    taxcalc.invoiceNo = orderInfo.Header.invoiceno;
                    int count = 0;
                    foreach (dynamic line in orderInfo.Detail)
                    {
                        OrderLine current;
                        current.index = count;
                        current.itemID = (string)line.Itemcode;
                        current.qty = (decimal)line.Qty;
                        current.price = (decimal)line.Price;
                        current.TIC = (int)line.TICCode; //"11010"
                        count++;
                        AllLines.Add(current);

                    }
                    string curCertID = (string)orderInfo.Header.CertId;
                    if (curCertID.Length > 0 && curCertID != "NA")
                    {
                        taxcalc.exemptCert = new ExemptionCertificate();
                        taxcalc.exemptCert.CertificateID = curCertID;
                    }

                    taxcalc.setOrderInfo((string)orderInfo.Header.CustNum, (string)orderInfo.Header.Address, (string)orderInfo.Header.City, (string)orderInfo.Header.State, (string)orderInfo.Header.Zip, (decimal)orderInfo.Header.Frieght, (string)orderInfo.Header.OrderNum, AllLines, (string)orderInfo.Header.Warehouse);
                    taxcalc.taxLookUp(0);
                    Console.WriteLine("Lines added");
                }




                //Console.WriteLine(orderInfo.Detail[0].Itemcode);
                // -----------------------------------------
                //string sql2ConnString;
                //List<string> historyInvoices = new List<string>();
                //SqlConnection sql2Conn;
                //SqlDataReader reader;
                //sql2ConnString = "Data Source=sql_core;Initial Catalog=MAS_KEN;Integrated Security=True";
                //sql2Conn = new SqlConnection(sql2ConnString);
                //SqlCommand cmd = new SqlCommand();
                //SqlCommand cmd1 = new SqlCommand();
                ////// cmd.CommandText = "SELECT  [InvoiceNo],CertId  FROM [MAS_KEN].[dbo].[AR_InvoiceHistoryHeader] ihh (nolock)LEFT JOIN info2.[KencoveTax].[dbo].[KT_ExemptCert] ec(nolock)on KENCOVEREPORT.[dbo].[CustomerNoConverter](ihh.CustomerNo) = KENCOVEREPORT.[dbo].[CustomerNoConverter](ec.CustomerNo) and ihh.ShipToState = ec.PurchaserState where InvoiceDate between '2016-12-01 00:00:00.000' and '2016-12-31 00:00:00.000'"; //"SELECT[InvoiceNo] FROM[MAS_KEN].[dbo].[AR_InvoiceHistoryHeader](NOLOCK) where InvoiceNo = '0440441'"; 
                //// cmd.CommandText = "SELECT  [InvoiceNoT],CertId,Act  FROM [KencoveReport].[dbo].[KT_AR_InvoiceHistoryHeader] ihh (nolock)LEFT JOIN info2.[KencoveTax].[dbo].[KT_ExemptCert] ec(nolock)on KENCOVEREPORT.[dbo].[CustomerNoConverter](ihh.CustomerNo) = KENCOVEREPORT.[dbo].[CustomerNoConverter](ec.CustomerNo) and (ihh.ShipToState = ec.PurchaserState or ec.[ReasonForExemption] = 'Resale') where InvoiceDate between '2016-12-01 00:00:00.000' and '2016-12-31 00:00:00.000' and ihh.ShipToState = 'PA'  "; //ihh.SalesOrderNoT NOT IN ('03222000-TT','0322831') "SELECT[InvoiceNo] FROM[MAS_KEN].[dbo].[AR_InvoiceHistoryHeader](NOLOCK) where InvoiceNo = '0440441'"; 
                //cmd1.CommandText = "EXEC KencoveReport.[dbo].[KT_Fill_Cache_TaxToFile]";

                //cmd1.CommandType = CommandType.Text;
                //cmd1.Connection = sql2Conn;
                //sql2Conn.Open();
                //reader = cmd1.ExecuteReader();
                //reader.Close();
                //cmd.CommandText = "SELECT  [InvoiceNoT],[CertId],[Act],[IncludeFreight]FROM [KencoveReport].[dbo].[KT_Cache_TaxToFile]";
                ////cmd.CommandText = "EXEC KencoveReport.dbo.KT_InvoiceInfoSelect";

                //cmd.CommandType = CommandType.Text;
                //cmd.Connection = sql2Conn;
                ////sql2Conn.Open();
                //reader = cmd.ExecuteReader();
                //Object[] values = new Object[reader.FieldCount];
                //while (reader.HasRows)
                //{
                //    if (reader.Read())
                //    {
                //        string curInvoice = "";
                //        string curCertID;
                //        reader.GetValues(values);
                //        curInvoice = (values[0] == DBNull.Value) ? string.Empty : (string)values[0];
                //        string f = (values[3] == DBNull.Value) ? string.Empty : (string)values[3];
                //        TaxCalculator taxCalc = new TaxCalculator();
                //        taxCalc.setOrderInfoFromInvoice(curInvoice, f);
                //        curCertID = (values[1] == DBNull.Value) ? string.Empty : (string)values[1];
                //        string act = (values[2] == DBNull.Value) ? string.Empty : (string)values[2];
                //        if (curCertID.Length > 0 && act == "Use Exemption")
                //        {
                //            taxCalc.exemptCert = new ExemptionCertificate();
                //            taxCalc.exemptCert.CertificateID = curCertID;
                //        }
                //        taxCalc.taxLookUp(0);
                //    }
                //    else
                //        break;
                //}


                // ------------------------------------------ 0440096

                //string sql2ConnString;
                //List<string> historyInvoices = new List<string>();
                //SqlConnection sql2Conn;
                //SqlDataReader reader;
                //sql2ConnString = "Data Source=sql_core;Initial Catalog=MAS_KEN;Integrated Security=True";
                //sql2Conn = new SqlConnection(sql2ConnString);
                ////SqlCommand cmd = new SqlCommand();
                ////cmd.CommandText = "SELECT   [CustomerNo] ,[State] ,[ReasonForExemption] ,[taxID] ,[taxIDtype] ,[firstName] ,[lastName] ,[address1] ,[city] ,[PurchaserState] ,[zip] ,[BusinessType] ,[ReasonForExemptionValue] FROM [KencoveReport].[dbo].KT_CertsNotUploaded (NOLOCK) "; //"SELECT[InvoiceNo] FROM[MAS_KEN].[dbo].[AR_InvoiceHistoryHeader](NOLOCK) where InvoiceNo = '0440441'"; 
                ////cmd.CommandType = CommandType.Text;
                ////cmd.Connection = sql2Conn;
                ////sql2Conn.Open();
                ////reader = cmd.ExecuteReader();
                ////Object[] values = new Object[reader.FieldCount];
                ////string[] states = new string[1];
                //TaxCalculator taxCalc = new TaxCalculator();
                //taxCalc.deleteAllCert("0001327");
                ////taxCalc.getCertInfo("96623");
                //while (reader.HasRows)
                //{
                //    if (reader.Read())
                //    {

                //        //string curInvoice = "";
                //        reader.GetValues(values);
                //        //taxCalc.deleteAllCert((values[0] == DBNull.Value) ? "" : (string)values[0]);
                //        states[0] = (values[1] == DBNull.Value) ? "" : (string)values[1];
                //        taxCalc.AddNewCert((values[0] == DBNull.Value) ? "" : (string)values[0],
                //            (values[4] == DBNull.Value) ? "" : (string)values[4],
                //            states, (values[3] == DBNull.Value) ? "" : (string)values[3], (values[5] == DBNull.Value) ? "" : (string)values[5], (values[6] == DBNull.Value) ? "" : (string)values[6],
                //            (values[0] == DBNull.Value) ? "" : (string)values[7],
                //            (values[8] == DBNull.Value) ? "" : (string)values[8], (values[9] == DBNull.Value) ? "" : (string)values[9], (values[10] == DBNull.Value) ? "" : (string)values[10],
                //            (values[11] == DBNull.Value) ? "" : (string)values[11], (values[12] == DBNull.Value) ? "" : (string)values[12]);
                //        //taxCalc.AddNewCert(states, "316-06-7764", "Mike", "Gott", "7053 W 1400 S", "Remington", "PA", "47977", "FM", "Farm");
                //    }
                //    else
                //        break;
                //}



                //taxCalc.setOrderInfo("0432306");

                //taxCalc.taxLookUp();

                Console.WriteLine();

            }
        }
        public class TaxCalculator
        {
            public string apiLoginId;
            public string apiKey;
            public TaxCloud tc;
            public TaxInfo orderInfo;
            public bool deliveredBySeller;
            public ExemptionCertificate exemptCert;
            public CartItem[] cartItems;
            public Address origin;
            public Address dest;
            public decimal totalTax;
            public LookupRsp response;
            public bool history;
            public string invoiceNo;
            public Certificate cert;
            
            public TaxCalculator()
            {
                apiLoginId = "3745A0B0";
                apiKey = "D56AA6AD-96B7-42CB-A9F6-88537D849058";
                tc = new TaxCloud();
                orderInfo = new TaxInfo();
                deliveredBySeller = false;
                exemptCert = null;
                origin = new Address();
                dest = new Address();
                totalTax = 0;
                history = false;


            }
            
            public void insertTaxDetail(int taxHeaderId,string itemcode,decimal unitPrice,decimal qty,decimal extendedPrice,decimal itemtax,int tic)
            {
                string info2ConnString;
                SqlConnection info2Conn;
               
                bool status = true;
                int HeaderId = 0;
                if(qty < 1)
                {
                    qty = 1;
                }

                info2ConnString = "Data Source=info2;Initial Catalog=KencoveTax;Integrated Security=True";
                info2Conn = new SqlConnection(info2ConnString);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.KT_InsertTaxDetail ";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@TaxHeaderID", SqlDbType.Int);
                cmd.Parameters.Add("@itemCode", SqlDbType.VarChar);
                cmd.Parameters.Add("@UnitPrice", SqlDbType.Decimal);
                cmd.Parameters.Add("@Quantity", SqlDbType.Decimal);
                cmd.Parameters.Add("@ExtendedPrice", SqlDbType.Decimal);
                cmd.Parameters.Add("@ItemTax", SqlDbType.VarChar);
                cmd.Parameters.Add("@TICCode", SqlDbType.Int);

                cmd.Parameters["@TaxHeaderID"].Value = taxHeaderId;
                cmd.Parameters["@itemCode"].Value = itemcode;
                cmd.Parameters["@UnitPrice"].Value = unitPrice;
                cmd.Parameters["@Quantity"].Value = qty;
                cmd.Parameters["@ExtendedPrice"].Value = extendedPrice;
                cmd.Parameters["@ItemTax"].Value = itemtax;
                cmd.Parameters["@TICCode"].Value = tic;
                cmd.Connection = info2Conn;
                if (taxHeaderId >0 && !itemcode.Equals("/C") && unitPrice >0)
                {
                    try
                    {
                        info2Conn.Open();
                        if(!itemcode.Equals("ShippingCost") || unitPrice !=0)
                        cmd.ExecuteNonQuery();
                    }
                    catch(Exception e)
                    {
                        status = false;
                    }
                    finally
                    {
                        info2Conn.Close();
                    }
                }
               
            }
            public int insertTaxHeader()
            {
                string info2ConnString;
                SqlConnection info2Conn;
                SqlDataReader reader;
                bool status = true;
                int HeaderId = 0;

                info2ConnString = "Data Source=info2;Initial Catalog=KencoveTax;Integrated Security=True";
                info2Conn = new SqlConnection(info2ConnString);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "dbo.KT_InsertTaxHeader ";
                    
                    //"INSERT INTO [KencoveTax].[dbo].[KT_ExemptCert](CertID,CustomerNo,firstName,lastName,city,zip) values('"
                    //+ exemptCert.CertificateID + "','" + orderInfo.customerId + "','" + exemptCert.Detail.PurchaserFirstName + "','" + exemptCert.Detail.PurchaserLastName +
                    //"','" + exemptCert.Detail.PurchaserCity + "','" + exemptCert.Detail.PurchaserZip + "')";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@InvoiceNo",SqlDbType.VarChar);
                cmd.Parameters.Add("@SalesOrderNo",SqlDbType.VarChar);
                cmd.Parameters.Add("@CustomerNo",SqlDbType.VarChar);
                cmd.Parameters.Add("@WarehouseCode", SqlDbType.VarChar);
                cmd.Parameters.Add("@Address1", SqlDbType.VarChar);
                cmd.Parameters.Add("@State", SqlDbType.VarChar);
                cmd.Parameters.Add("@Zip", SqlDbType.VarChar);
                cmd.Parameters.Add("@TotalTax",SqlDbType.Decimal);
                cmd.Parameters.Add("@InvoiceDate", SqlDbType.DateTime);
                cmd.Parameters.Add("@CertID", SqlDbType.VarChar);

                cmd.Parameters["@InvoiceNo"].Value = invoiceNo;
                cmd.Parameters["@SalesOrderNo"].Value = orderInfo.cartID;
                cmd.Parameters["@CustomerNo"].Value = orderInfo.customerId;
                cmd.Parameters["@WarehouseCode"].Value =orderInfo.warehouse;
                cmd.Parameters["@Address1"].Value = orderInfo.DestAddress1;
                cmd.Parameters["@State"].Value = orderInfo.DestState;
                cmd.Parameters["@Zip"].Value = orderInfo.DestZip5;
                cmd.Parameters["@TotalTax"].Value = totalTax;
                cmd.Parameters["@InvoiceDate"].Value = orderInfo.InvoiceDate;
                cmd.Parameters["@CertID"].Value = (exemptCert == null) ? "": exemptCert.CertificateID;
                    
                cmd.Connection = info2Conn;
                
                try
                {
                    info2Conn.Open();
                   reader =  cmd.ExecuteReader();
                    Object[] values = new Object[reader.FieldCount];
                    if (reader.Read())
                    {
                        int val = reader.GetValues(values);
                        HeaderId = (values[0] == DBNull.Value) ? 0 : (int)values[0];

                    }
                        status = true;
                }
                catch( Exception e)
                {

                    status = false;
                }
                return HeaderId;
            }
            public decimal taxLookUp(int capture = 0)
            {
                totalTax = 0;
                int taxheaderId=0;
                int emptyCart = 0;
                if(cartItems[0] == null || cartItems.Length == 0)
                {
                    emptyCart = 1;
                }
                if(emptyCart == 0)
                { 
                   
                    response =tc.LookupForDate(apiLoginId, apiKey, orderInfo.customerId, orderInfo.cartID, cartItems, origin, dest, (DateTime.Today).AddMonths(-1), exemptCert, deliveredBySeller);
                }
                if( emptyCart == 1)
                {
                    Console.WriteLine("Empty Cart {0}", orderInfo.cartID);
                }
               else  if (response.ResponseType == MessageType.Error ) 
                {
           
                        foreach (ResponseMessage message in response.Messages)
                        {
                            Console.WriteLine("Error : {0} - {1}",
                            message.ResponseType.ToString(), message.Message);
                        }
                 
                }
                else
                {
                  
                    int lineCount = 0;
                    foreach (CartItemResponse cir in response.CartItemsResponse)
                    {
                        Console.WriteLine("Cart Item {0} : Tax Amount {1}",
                        cir.CartItemIndex, cir.TaxAmount.ToString());

                        totalTax += System.Convert.ToDecimal(cir.TaxAmount.ToString());
                    
                    }
                    taxheaderId = insertTaxHeader();
                    foreach (CartItemResponse cir in response.CartItemsResponse)
                    {
                   
                  
                        if (taxheaderId > 0)
                        { insertTaxDetail(taxheaderId, cartItems[lineCount].ItemID, (decimal)cartItems[lineCount].Price, (decimal)cartItems[lineCount].Qty, (decimal)(cartItems[lineCount].Qty * cartItems[lineCount].Price), System.Convert.ToDecimal(cir.TaxAmount.ToString()),(int)cartItems[lineCount].TIC); }

                     
                        lineCount++;
                    }
                    if( capture == 1)
                    {
                        // tc.AuthorizedWithCapture(apiLoginId, apiKey, orderInfo.customerId, orderInfo.cartID, orderInfo.cartID, (DateTime.Today).AddMonths(-1), (DateTime.Today).AddMonths(-1));
                        tc.AuthorizedWithCapture(apiLoginId, apiKey, orderInfo.customerId, orderInfo.cartID, orderInfo.cartID, (DateTime.Today).AddMonths(-1), (DateTime.Today).AddMonths(-1));
                    }
                }

                Console.WriteLine("{1} Total Tax {0}", totalTax,orderInfo.cartID);
                return totalTax;
            }
            public void setOrderInfo(string customerNo, string shipToAddress, string shipToCity, string shipToState, string shipToZipcode, decimal freightAmt, string salesOrderNo, List<OrderLine> orderlines,string warehouse = "PA")
            {
                //orderInfo.setHeaderInfo(customerNo,shipToAddress, shipToCity, shipToState, shipToZipcode, freightAmt, salesOrderNo);
                //orderInfo.setDetailInfo(orderlines);
                orderInfo.TaxFromOrder(customerNo, shipToAddress, shipToCity, shipToState, shipToZipcode, freightAmt, salesOrderNo, orderlines,warehouse);
               
                origin.Address1 = orderInfo.OriginAddress1;
                origin.City = orderInfo.OriginCity;
                origin.State = orderInfo.OriginState;
                origin.Zip5 = orderInfo.OriginZip5;
                origin.Zip4 = orderInfo.OriginZip4;

                dest.Address1 = orderInfo.DestAddress1;
                dest.City = orderInfo.DestCity;
                dest.State = orderInfo.DestState;
                dest.Zip5 = orderInfo.DestZip5;
                dest.Zip4 = orderInfo.DestZip4;
                int wholeQty = 1;
                int index = 0;
                cartItems = new CartItem[orderInfo.OrderIndex + 1];
                foreach (OrderLine line in orderInfo.Lines)
                {
                    if((int)line.qty >1)
                    {
                        wholeQty = (int)line.qty;
                    }
                    else
                    {
                        wholeQty = 1;
                    }
                    cartItems[index] = new CartItem();
                    cartItems[index].Index = index;
                    cartItems[index].TIC = line.TIC;
                    cartItems[index].ItemID = line.itemID;
                    cartItems[index].Price = (double)line.price;
                    cartItems[index].Qty = wholeQty;
                    index++;

                }


            }
            public void setOrderInfoFromInvoice(string OrderID, string f,int infoType = 1)
            {
                if (infoType == 1)
                {
                    orderInfo.TaxFromInvoice(OrderID,f);
                    invoiceNo = OrderID;
                }
                else
                {
                    //add tax from sales call
                }
                origin.Address1 = orderInfo.OriginAddress1;
                origin.City = orderInfo.OriginCity;
                origin.State = orderInfo.OriginState;
                origin.Zip5 = orderInfo.OriginZip5;
                origin.Zip4 = orderInfo.OriginZip4;

                dest.Address1 = orderInfo.DestAddress1;
                dest.City = orderInfo.DestCity;
                dest.State = orderInfo.DestState;
                dest.Zip5 = orderInfo.DestZip5;
                dest.Zip4 = orderInfo.DestZip4;

                int index = 0;
                cartItems = new CartItem[orderInfo.OrderIndex + 1];
                int wholeQty = 1;
                foreach (OrderLine line in orderInfo.Lines)
                {
                    if ((int)line.qty > 1)
                    {
                        wholeQty = (int)line.qty;
                    }
                    else
                    {
                        wholeQty = 1;
                    }
                    cartItems[index] = new CartItem();
                    cartItems[index].Index = index;
                    cartItems[index].TIC = line.TIC;
                    cartItems[index].ItemID = line.itemID;
                    cartItems[index].Price = (double)line.price;
                    cartItems[index].Qty = wholeQty;
                    index++;

                }
            }
            public void setSingleCertificate()
            {
                //ExemptionCertificate exemptCert = null;
                exemptCert = new ExemptionCertificate();

                exemptCert.Detail = new ExemptionCertificateDetail();
             
                exemptCert.Detail.SinglePurchase = true;
                exemptCert.Detail.SinglePurchaseOrderNumber = orderInfo.cartID;
                ExemptState[] exemptStates = new ExemptState[1];
                exemptStates[0] = new ExemptState();

                exemptStates[0] = new ExemptState();
                exemptStates[0].StateAbbr = State.PA;
                exemptStates[0].ReasonForExemption = "Farming";
                exemptStates[0].IdentificationNumber = "111-11-1111";

                exemptCert.Detail.ExemptStates = exemptStates;
                exemptCert.Detail.PurchaserTaxID = new TaxID();
                exemptCert.Detail.PurchaserTaxID.TaxType = TaxIDType.FEIN;
                exemptCert.Detail.PurchaserTaxID.IDNumber = "111-11-1111";
                exemptCert.Detail.PurchaserFirstName = "Thistle Creek Farm";
                exemptCert.Detail.PurchaserLastName = "LLC";
                exemptCert.Detail.PurchaserAddress1 = "10702 Jonestown Rd";
                exemptCert.Detail.PurchaserCity = "Annville";
                exemptCert.Detail.PurchaserState = State.PA;
                exemptCert.Detail.PurchaserZip = "17003";
                exemptCert.Detail.PurchaserBusinessType = BusinessType.Agricultural_Forestry_Fishing_Hunting;
                exemptCert.Detail.PurchaserExemptionReason = ExemptionReason.AgriculturalProduction;
                exemptCert.Detail.PurchaserExemptionReasonValue = "Farm#";
              
            }
            public void getCertInfo()
            {
                GetCertificatesRsp custCerts = tc.GetExemptCertificates(apiLoginId, apiKey, orderInfo.customerId);
                //custCerts.ExemptCertificates.
            }
            public void getCertInfo(string custID)
            {
                GetCertificatesRsp custCerts = tc.GetExemptCertificates(apiLoginId, apiKey, custID);
                //custCerts.ExemptCertificates.
                foreach(var c in custCerts.ExemptCertificates)
                {
                    Console.WriteLine(c.CertificateID);
                }
            }
            public bool AddNewCert(string customerNo, string taxType,string[] states,string taxID,string custFirstName,string custLastName,string custAddress,string city,string purchaserState, string zip,string bType ,string exemptReasonValue  )
            {
                bool certExists = false;
                string dupCertID = "";
                string expReason = "";
                exemptCert = new ExemptionCertificate();

                exemptCert.Detail = new ExemptionCertificateDetail();

                exemptCert.Detail.SinglePurchase = false;
               // exemptCert.Detail.SinglePurchaseOrderNumber = orderInfo.cartID;
                ExemptState[] exemptStates = new ExemptState[states.Length];
                int stateCount = 0;
                foreach (string s in states)
                {
                    exemptStates[stateCount] = new ExemptState();
                    if(s== "ID")
                    {
                        exemptStates[stateCount].StateAbbr = State.ID;
                    }
                    else if(s=="MO")
                    {
                        exemptStates[stateCount].StateAbbr = State.MO;
                    }
                    else if(s=="IN")
                    {
                        exemptStates[stateCount].StateAbbr = State.IN;
                    }
                    else if(s=="KY")
                    {
                        exemptStates[stateCount].StateAbbr = State.KY;
                    }
                    else if (s == "GA")
                    {
                        exemptStates[stateCount].StateAbbr = State.GA;
                    }
                    else if (s == "OH")
                    {
                        exemptStates[stateCount].StateAbbr = State.OH;
                    }

                    else
                    {
                        exemptStates[stateCount].StateAbbr = State.PA;
                    }
                    //exemptStates[stateCount].ReasonForExemption = exemptReasonValue;
                    //exemptStates[stateCount].IdentificationNumber = "111-11-1111";
                }
                exemptCert.Detail.ExemptStates = exemptStates;
                
                exemptCert.Detail.PurchaserTaxID = new TaxID();
                if (taxType == "StateIssued")
                {
                    exemptCert.Detail.PurchaserTaxID.TaxType = TaxIDType.StateIssued;
                    exemptCert.Detail.PurchaserTaxID.StateOfIssue = purchaserState;
                }
                else if(taxType =="SSN")
                {
                    exemptCert.Detail.PurchaserTaxID.TaxType = TaxIDType.SSN;
                }
                else
                {
                    exemptCert.Detail.PurchaserTaxID.TaxType = TaxIDType.FEIN;
                }
               
                exemptCert.Detail.PurchaserTaxID.IDNumber = taxID;
                exemptCert.Detail.PurchaserFirstName = custFirstName;
                exemptCert.Detail.PurchaserLastName = custLastName;
                exemptCert.Detail.PurchaserAddress1 = custAddress;
                exemptCert.Detail.PurchaserCity = city;

                if (purchaserState == "ID")
                    exemptCert.Detail.PurchaserState = State.ID;
                else if (purchaserState == "MO")
                    exemptCert.Detail.PurchaserState = State.MO;
                else if (purchaserState == "IN")
                    exemptCert.Detail.PurchaserState = State.IN;
                else if(purchaserState == "KY")
                    exemptCert.Detail.PurchaserState = State.KY;
                else if (purchaserState == "GA")
                    exemptCert.Detail.PurchaserState = State.GA;
                else if (purchaserState == "OH")
                    exemptCert.Detail.PurchaserState = State.OH;
                else
                    exemptCert.Detail.PurchaserState = State.PA;


                exemptCert.Detail.PurchaserZip = zip;
                //if statement for business type
                if(bType == "GOV" || bType == "Government")
                {
                    exemptCert.Detail.PurchaserBusinessType = BusinessType.Government;
                    exemptCert.Detail.PurchaserExemptionReason = ExemptionReason.StateOrLocalGovernmentName;
                    exemptCert.Detail.PurchaserExemptionReasonValue = exemptReasonValue;
                    expReason = "StateOrLocalGovernmentName";
                }
                else if(bType == "NP"|| bType == "NonprofitOrganization")
                { 
                    exemptCert.Detail.PurchaserBusinessType = BusinessType.NonprofitOrganization;
                    exemptCert.Detail.PurchaserExemptionReason = ExemptionReason.Other;
                    exemptCert.Detail.PurchaserExemptionReasonValue = "NonprofitOrganization";//"NonprofitOrganization#";
                    expReason = "Other";
                }
                else if(bType == "RS"|| bType == "RetailTrade")
                {
                    exemptCert.Detail.PurchaserBusinessType = BusinessType.RetailTrade;
                    exemptCert.Detail.PurchaserExemptionReason = ExemptionReason.Resale;
                    exemptCert.Detail.PurchaserExemptionReasonValue = exemptReasonValue;//exemptReasonValue;
                    expReason = "Resale";
                }
                
                else if(bType== "WholesaleTrade")
                {
                    exemptCert.Detail.PurchaserBusinessType = BusinessType.WholesaleTrade;
                    exemptCert.Detail.PurchaserExemptionReason = ExemptionReason.Resale;
                    exemptCert.Detail.PurchaserExemptionReasonValue = exemptReasonValue;//exemptReasonValue;
                    expReason = "Resale";
                }
                else if(bType == "FM" || bType == "Agricultural_Forestry_Fishing_Hunting")
                {
                    exemptCert.Detail.PurchaserBusinessType = BusinessType.Agricultural_Forestry_Fishing_Hunting;
                    exemptCert.Detail.PurchaserExemptionReason = ExemptionReason.AgriculturalProduction;
                    exemptCert.Detail.PurchaserExemptionReasonValue = exemptReasonValue;
                    expReason = "AgriculturalProduction";
                }
                else
                {
                    exemptCert.Detail.PurchaserBusinessType = BusinessType.Other;
                    exemptCert.Detail.PurchaserExemptionReason = ExemptionReason.Other;
                    exemptCert.Detail.PurchaserExemptionReasonValue = exemptReasonValue;
                    expReason = "Other";
                }
                //---- check to see if cert already exists orderInfo.customerId
                GetCertificatesRsp existingCerts = tc.GetExemptCertificates(apiLoginId, apiKey, customerNo );
                foreach (ExemptionCertificate cert in existingCerts.ExemptCertificates)
                {
                    if (cert.Detail.PurchaserState.Equals(exemptCert.Detail.PurchaserState))
                    {
                        Console.WriteLine("Certificate exists with ID:{0}", cert.CertificateID);
                        certExists = true;
                        dupCertID = cert.CertificateID;
                    }
                }
                //end check cert check
                if(!certExists)
                { 
                    AddCertificateRsp addCertRsp = tc.AddExemptCertificate(apiLoginId,apiKey, customerNo, exemptCert);
                    
                    if (addCertRsp.ResponseType == MessageType.OK)
                    {
                        exemptCert.CertificateID = addCertRsp.CertificateID;
                         Console.WriteLine("New Cert inserted with ID: {0}", exemptCert.CertificateID);
                        return insertNewCert(customerNo,states[0], purchaserState, expReason,taxID,taxType);
                       
                    }
                    else
                    {
                        Console.WriteLine(addCertRsp.Messages[0].Message);
                        return false;
                    
                    }
                }
                else
                {
                    exemptCert = new ExemptionCertificate();
                    exemptCert.CertificateID = dupCertID;
                    return false;
                }
                
            }
            public bool insertNewCert(string customerNo,string state,string purchaserstate, string ReasonForEx,string taxId,string taxType )
            {
                string info2ConnString;
                SqlConnection info2Conn;
                SqlDataReader reader;
                bool status = true;
               
                info2ConnString = "Data Source=info2;Initial Catalog=KencoveTax;Integrated Security=True";
                info2Conn = new SqlConnection(info2ConnString);
                SqlCommand cmd = new SqlCommand();
                //orderInfo.customerId
                cmd.CommandText = "INSERT INTO [KencoveTax].[dbo].[KT_ExemptCert](CertID,CustomerNo,firstName,lastName,city,zip,State,PurchaserState,ReasonForExemption,taxID,taxtype) values('"
                    + exemptCert.CertificateID +"','"+customerNo + "','"+exemptCert.Detail.PurchaserFirstName+ "','"+exemptCert.Detail.PurchaserLastName+
                    "','"+exemptCert.Detail.PurchaserCity+"','"+exemptCert.Detail.PurchaserZip+"','"+state+"','"+ purchaserstate + "','"+ReasonForEx+"','"+taxId+"','"+taxType+"')";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = info2Conn;
                try
                { 
                    info2Conn.Open();
                    cmd.ExecuteNonQuery();
                    status = true;
                 }
                catch(Exception e)
                {
                    
                    status = false;
                }

              
                info2Conn.Close();
                    return status;
            }
            public void  deleteAllCert(string custID)
            {
                //0749380
                GetCertificatesRsp existingCerts = tc.GetExemptCertificates(apiLoginId, apiKey, custID);
          
                foreach (ExemptionCertificate cert in existingCerts.ExemptCertificates)
                {
                   
                        Console.WriteLine("Certificate exists with ID:{0}, certificate deleted", cert.CertificateID);
                        tc.DeleteExemptCertificate(apiLoginId, apiKey, cert.CertificateID);
                    
                }
            }

        }
        public struct Certificate
        {
            public string certID;
            public string[] states;
            public string taxID;
            public string custFirstName;
            public string custLastName;
            public string custAddress;
            public string city;
            public string state;
            public string zip;
            public string bType;
            public string exemptReasonValue;
        }

        public class TaxInfo
        {
            public string OriginAddress1;
            public string OriginCity;
            public string OriginState;
            public string OriginZip5;
            public string OriginZip4;
            public string OriginZipWhole;
            public bool deliveredBySeller;
            public string cartID;
            public decimal freight;
            public int OrderIndex;
            public string warehouse;

            public string DestAddress1;
            public string DestCity;
            public string DestState;
            public string DestZip5;
            public string DestZip4;
            public string DestZipWhole;

            public string sql2ConnString;
            SqlConnection sql2Conn;
            SqlDataReader reader;
            public string customerId;
            public DateTime InvoiceDate;
            public List<OrderLine> Lines = new List<OrderLine>();
            public TaxInfo()
            {
                OriginAddress1 = "344 Kendall Rd";
                OriginCity = "Blairsville";
                OriginState = "PA";
                OriginZip5 = "15717";
                OriginZip4 = "";
                OriginZipWhole = "";
                deliveredBySeller = false;

                DestAddress1 = "";
                DestCity = "";
                DestState = "";
                DestZip4 = "";
                DestZip5 = "";
                DestZipWhole = "";

                customerId = "";
                sql2ConnString = "Data Source=sql_core;Initial Catalog=MAS_KEN;Integrated Security=True";
                InvoiceDate = DateTime.Today;
                Lines = new List<OrderLine>();
            }

            // Loads order header info from invoice, used for testing
          
            public void setHeaderInfoFromInvoiceNo(string invoiceNo)
            {
                //SQL Connection Setup
                sql2ConnString = "Data Source=sql_core;Initial Catalog=MAS_KEN;Integrated Security=True";
                sql2Conn = new SqlConnection(sql2ConnString);
                SqlCommand cmd = new SqlCommand();
               // cmd.CommandText = " Select [CustomerNo],[ShipToAddress1],[ShipToCity],[ShipToState],[ShipToZipCode],[FreightAmt],[SalesOrderNo],[UDF_WAREHOUSE_GROUP] FROM [MAS_KEN].[dbo].[AR_InvoiceHistoryHeader] aihh (NOLOCK)JOIN[dbo].[IM_Warehouse] iw(NOLOCK)on aihh.warehouseCode = iw.WarehouseCode  where InvoiceNO = '" + invoiceNo+"'";
                cmd.CommandText = " Select [CustomerNo],[ShipToAddress1],[ShipToCity],[ShipToState],[ShipToZipCode],[FreightAmt],[SalesOrderNoT],[UDF_WAREHOUSE_GROUP],[InvoiceDate] FROM [KencoveReport].[dbo].[KT_AR_InvoiceHistoryHeader] aihh (NOLOCK)JOIN[dbo].[IM_Warehouse] iw(NOLOCK)on aihh.warehouseCode = iw.WarehouseCode  where InvoiceNOT = '" + invoiceNo + "'";

                cmd.CommandType = CommandType.Text;
                cmd.Connection = sql2Conn;
                sql2Conn.Open();
                reader = cmd.ExecuteReader();

                //Read SQL Data
                int val;
                Object[] values = new Object[reader.FieldCount];
                if (reader.Read())
                {
                    
                    val = reader.GetValues(values);
                    customerId = (values[0] == DBNull.Value) ? "" : (string)values[0];
                    DestAddress1 = (values[1] == DBNull.Value) ? "" : (string)values[1];
                    DestCity = (values[2] == DBNull.Value) ? "" : (string)values[2];
                    DestState = (values[3] == DBNull.Value) ? "" : (string)values[3];
                    DestZipWhole = DestCity = (values[4] == DBNull.Value) ? "" : (string)values[4];
                    DestZip5 = (DestZipWhole.Length < 5) ? "" : DestZipWhole.Substring(0, 5);
                    DestZip4 = (DestZipWhole.Length < 10)?"": DestZipWhole.Substring(DestZipWhole.IndexOf("-") + 1, 4);
                    freight = (values[5] == DBNull.Value) ? 0m : (decimal)values[5];
                    cartID = (values[6] == DBNull.Value) ? "" : (string)values[6];
                    warehouse = (values[7] == DBNull.Value) ? "" : (string)values[7];
                    InvoiceDate = (values[8] == DBNull.Value) ? DateTime.Today : (DateTime)values[8];
                    if(warehouse == "IN")
                    {
                        OriginAddress1 = "113 W 5th St";
                        OriginCity = "Earl Park";
                        OriginState = "IN";
                        OriginZip5 = "47942";
                    }
                    else if(warehouse == "MO")
                    {
                        OriginAddress1 = "11409 E 218TH ST";
                        OriginCity = "Peculiar";
                        OriginState = "MO";
                        OriginZip5 = "64078";
                    }
                    else if(warehouse == "ID")
                    {
                        OriginAddress1 = "3520 Arthur St";
                        OriginCity = "Caldwell";
                        OriginState = "ID ";
                        OriginZip5 = "83605";
                    }
                    else if (warehouse == "GA")
                    {
                        OriginAddress1 = "2643 McRae Highway";
                        OriginCity = "EASTMAN";
                        OriginState = "GA ";
                        OriginZip5 = "31023";
                    }
                    else if(warehouse == "PP")
                    {
                        OriginAddress1 = "713 E Austin Blvd";
                        OriginCity = "Nevada";
                        OriginState = "MO ";
                        OriginZip5 = "64772";
                    }
                    else if (warehouse == "KIN")
                    {
                        OriginAddress1 = "2945 RIVERS BRIDGE ROAD";
                        OriginCity = "EHRHARDT";
                        OriginState = "SC";
                        OriginZip5 = "29081";
                    }
                }

                reader.Close();
                sql2Conn.Close();

            }
            public void setHeaderInfo(string customerNo,string shipToAddress,string shipToCity,string shipToState,string shipToZipcode,decimal freightAmt,string salesOrderNo,string warehouseCode)
            {
                customerId = customerNo;
                DestAddress1 = shipToAddress;
                DestCity = shipToCity;
                DestState = shipToState;
                DestZipWhole = shipToZipcode;
                DestZip5 = (DestZipWhole.Length < 5) ? "" : DestZipWhole.Substring(0, 5);
                DestZip4 = (DestZipWhole.Length < 10) ? "" : DestZipWhole.Substring(DestZipWhole.IndexOf("-") + 1, 4);
                freight = freightAmt;
                cartID = salesOrderNo;
                warehouse = warehouseCode;

                if (warehouse == "IN")
                {
                    OriginAddress1 = "113 W 5th St";
                    OriginCity = "Earl Park";
                    OriginState = "IN";
                    OriginZip5 = "47942";
                }
                else if (warehouse == "MO")
                {
                    OriginAddress1 = "11409 E 218TH ST";
                    OriginCity = "Peculiar";
                    OriginState = "MO";
                    OriginZip5 = "64078";
                }
                else if (warehouse == "ID")
                {
                    OriginAddress1 = "3520 Arthur St";
                    OriginCity = "Caldwell";
                    OriginState = "ID ";
                    OriginZip5 = "83605";
                }
                else if (warehouse == "GA")
                {
                    OriginAddress1 = "2643 McRae Highway";
                    OriginCity = "EASTMAN";
                    OriginState = "GA ";
                    OriginZip5 = "31023";
                }
                else if (warehouse == "PP")
                {
                    OriginAddress1 = "713 E Austin Blvd";
                    OriginCity = "Nevada";
                    OriginState = "MO ";
                    OriginZip5 = "64772";
                }
                else if (warehouse == "KIN")
                {
                    OriginAddress1 = "2945 RIVERS BRIDGE ROAD";
                    OriginCity = "EHRHARDT";
                    OriginState = "SC";
                    OriginZip5 = "29081";
                }
            }
            //loads order details from invoice, used for testing
            public void setDetailInfoFromInvoiceNo(string invoiceNo)
            {
                sql2ConnString = "Data Source=sql_core;Initial Catalog=MAS_KEN;Integrated Security=True";
                sql2Conn = new SqlConnection(sql2ConnString);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT  ard.[ItemCode], QuantityShipped,UnitPrice,CAST(tc.TICCode as int) TICCODE FROM [KencoveReport].[dbo].[KT_AR_InvoiceHistoryDetail] (NOLOCK) ard LEFT JOIN info2.KencoveTax.dbo.KT_Item_TICCode tc on tc.ItemCode = ard.ItemCode where InvoiceNoT = '" + invoiceNo + "' and  ard.ItemType<>4 AND ard.ExplodedKitItem<>'C'";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sql2Conn;
                sql2Conn.Open();
                reader = cmd.ExecuteReader();
                int val;
                Object[] values = new Object[reader.FieldCount];
                OrderLine current;
                int count = 0;
                decimal calcPrice = 0;
                while (reader.HasRows)
                {
                    if(reader.Read())
                    {
                        val = reader.GetValues(values);
                        current.index = count;
                        current.itemID = (values[0] == DBNull.Value) ? "" : (string)values[0];
                        current.qty = (values[1] == DBNull.Value) ? 0 : (decimal)values[1];
                        if(current.qty%1 !=0)
                        {
                            calcPrice = current.qty * ((values[2] == DBNull.Value) ? 0m : (decimal)values[2]);
                            current.qty = 1;
                        }
                        else
                        {
                            calcPrice = ((values[2] == DBNull.Value) ? 0m : (decimal)values[2]);
                        }
                        current.price = calcPrice;
                        current.TIC = (values[3] == DBNull.Value) ? 0 : (int)values[3]; ;//"11010"
                        count++;
                        Lines.Add(current);
                    }
                    else
                    {
                        break;
                    }
                }
                OrderIndex = count;
                reader.Close();
                sql2Conn.Close();
            }
            public void setDetailInfo(List<OrderLine>  orderlines)
            {
                //OrderLine current;
                int count = 0;
                foreach (OrderLine line in orderlines)
                {
                    Lines.Add(line);
                    count++;
                }
                OrderIndex = count;
            }
            public void TaxFromOrder(string customerNo, string shipToAddress, string shipToCity, string shipToState, string shipToZipcode, decimal freightAmt, string salesOrderNo, List<OrderLine> orderlines,string warehouse)
            {
                setHeaderInfo( customerNo,  shipToAddress, shipToCity,  shipToState,  shipToZipcode, freightAmt,  salesOrderNo,warehouse);
                setDetailInfo(orderlines);

                OrderLine shipping;
                shipping.index = OrderIndex + 1;
                shipping.itemID = "ShippingCost";
                shipping.price = freight;
                shipping.qty = 1;
                shipping.TIC = 11010;

                Lines.Add(shipping);
            }
            public void TaxFromInvoice(string invoiceNo,string f)
            {
                setHeaderInfoFromInvoiceNo(invoiceNo);
                setDetailInfoFromInvoiceNo(invoiceNo);
                //add shipping line

                OrderLine shipping;
                shipping.index = OrderIndex + 1;
                shipping.itemID = "ShippingCost";
                shipping.price = freight;
                shipping.qty = 1;
                shipping.TIC = 11010;
                if (f == "1")
                {
                    Lines.Add(shipping);
                }

            }

        }
        public struct OrderLine
        {
            public int index;
            public int TIC;
            public string itemID;
            public decimal price;
            public decimal qty;

        }

    }
}
